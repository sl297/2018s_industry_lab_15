package ictgradschool.industry.designpatterns.ex01;

public class NestingShape extends Shape {

    public NestingShape(){
        super();
    }
    public NestingShape(int x, int y){
        super(x,y);
    }
    public NestingShape(int x, int y, int deltaX, int deltaY){
        super(x,y,deltaX,deltaY);
    }
    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height){
        super(x,y,deltaX,deltaY,width,height);
    }

    /**
     * Moves a NestingShape object (including its children) with the bounds specified by
     * arguments width and height. Remember that the NestingShape’s children only want
     * to move within the bounds of the NestingShape itself, rather than the whole screen.
     * @param width width of two-dimensional world.
     * @param height height of two-dimensional world.
     */
    public void move(int width, int height){
        super.move(width, height);

        for(Shape shape : this.children){
            shape.move(fWidth, fHeight);
        }
    }

    /**
     * Paints a NestingShape object by drawing a rectangle around the edge of its
     * bounding box. The NestingShape object's children are then painted.
     * @param painter the ictgradschool.industry.designpatterns.ex01.Painter object used for drawing.
     */
    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX, fY);
        for(Shape shape : this.children){
            shape.paint(painter);
        }
        // Reset the coordination to -fX, -fY
        painter.translate(-fX, -fY);
    }

    /**
     * Attempts to add a Shape to a NestingShape object. If successful, a two-way link is
     * established between the NestingShape and the newly added Shape. This method
     * throws an IllegalArgumentException if an attempt is made to add a Shape to a
     * NestingShape instance where the Shape argument is already a child within a
     * NestingShape instance. An IllegalArgumentException is also thrown when an
     * attempt is made to add a Shape that will not fit within the bounds of the proposed
     * NestingShape object.
     * @param child
     */
    public void add(Shape child){
        if(this.contains(child)){
            throw new IllegalArgumentException("Sorry, we could not find the child that should add in");
        }else{
           for(Shape childShape: this.children) {
//               if (childShape instanceof NestingShape && ((NestingShape)childShape).contains(child)) {
//                   throw new IllegalArgumentException("Sorry, we could not find the child that should add in");
//               }
               if(childShape instanceof NestingShape){
                   NestingShape nestingShape = (NestingShape)childShape;
                   if(nestingShape.contains(child)){
                       throw new IllegalArgumentException();
                   }
               }
           }
        }
        if((this.fX+this.getWidth() < child.fX + child.getWidth()) || (this.fX+this.getHeight() < child.fY+child.getHeight())){
            throw new IllegalArgumentException("Out of Bound");
        }
        this.children.add(child);// one way link this object with passed child Shape
        child.parent = this;// the child's parent must link to this object
    }

    /**
     * Removes a particular Shape from a NestingShape instance. Once removed, the
     * two-way link between the NestingShape and its former child is destroyed. This
     * method has no effect if the Shape specified to remove is not a child of the
     * NestingShape.
     * @param child
     */
    public void remove(Shape child){
        for(int i = 0; i<this.children.size(); i++){
            Shape shape = this.children.get(i);
            if(shape == child){
                shape.parent = null;
                this.children.remove(shape);
                return;
            }
        }
    }
    public Shape shapeAt(int index){
        if(index<0||index>this.children.size()){
           throw new IndexOutOfBoundsException("the value over the limitation");
        }
        Shape shape = this.children.get(index);
        return shape;
    }
    public int shapeCount(){
        int count = this.children.size();
        return count;
    }
    public int indexOf(Shape child){
        if(this.children.contains(child)){
            int index = this.children.indexOf(child);
            return index;
        }
        return -1;
    }

    /**
     * Returns true if the Shape argument is a child of the NestingShape object on which
     * this method is called, false otherwise.
     * @param child
     * @return
     */
    public boolean contains(Shape child){
        if(this.children.contains(child)){
            return true;
        }
        return false;
    }
}
